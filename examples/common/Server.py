#!/usr/bin/env python
# --------------------------------------------------------------------------- #
# import the various server implementations
# --------------------------------------------------------------------------- #
#from pymodbus.server.asynchronous import StartTcpServer
import random

from pymodbus.server.sync import StartTcpServer
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock, ModbusSparseDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from ExcelLib import RegistrInfo,Constans,ReportFromExcel
# --------------------------------------------------------------------------- #
# import the twisted libraries we need
# --------------------------------------------------------------------------- #
from twisted.internet.task import LoopingCall
# --------------------------------------------------------------------------- #
# import the payload builder
# --------------------------------------------------------------------------- #
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from collections import OrderedDict

from pymodbus.transaction import ModbusRtuFramer, ModbusBinaryFramer
# --------------------------------------------------------------------------- #
# configure the service logging
# --------------------------------------------------------------------------- #
import logging

FORMAT = ('%(asctime)-15s %(threadName)-15s'
          ' %(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.DEBUG)
# log.setLevel(logging.ERROR)


def updateReads(a):
    context = a[0]
    codeCommand = 0x6        # код команды 6 читать значение регистра
    slave_id = 0x00
    address=0
    values = context[slave_id].getValues(codeCommand, address, 4)
    decoder = BinaryPayloadDecoder.fromRegisters(values,
                                             byteorder=Endian.Little,
                                             wordorder=Endian.Little)
    value_64float=decoder.decode_64bit_float()
    log.debug("{0};{1};{2}".format(slave_id,address,value_64float))

def update_reads(a):
    log.debug("updating the context")
    context = a[0]
    fx = 0x6
    slave_id = 0x00
    address = 0x0
    for i in range(0,9):
        address=i
        values = context[slave_id].getValues(fx, address, 4)
        decoder = BinaryPayloadDecoder.fromRegisters(values,
                                                 byteorder=Endian.Little,
                                                 wordorder=Endian.Little)
        value_64float=decoder.decode_64bit_float()
        print("{0};{1};{2}".format(slave_id,address,value_64float))


def run_server():
    # ----------------------------------------------------------------------- #
    # build your payload
    # ----------------------------------------------------------------------- #
    builder = BinaryPayloadBuilder(byteorder=Endian.Little,
                                   wordorder=Endian.Little)
    builderSingle=BinaryPayloadBuilder(byteorder=Endian.Little,
                                   wordorder=Endian.Little)
    context = ModbusServerContext(single=False)
    const=Constans.Constans()
    report=ReportFromExcel.ReportFromExcel()
    for slaveId in range(1,4):
        registers = []
        idRegistr = 1
        for i in range(1,const.MAX_TAGS):
            if slaveId==2:
                real_value = slaveId+random.randint(1, 100)/125
                builder.add_64bit_float(real_value)
                builderSingle.add_64bit_float(real_value)
                registers.append(RegistrInfo.RegistrInfo(idRegistr,real_value,builderSingle.to_registers()))
                idRegistr+=const.DOUBLE_SIZE
            if slaveId==1:
                real_value = slaveId + random.randint(1, 100) / 125
                builder.add_32bit_float(real_value)
                builderSingle.add_32bit_float(real_value)
                registers.append(RegistrInfo.RegistrInfo(idRegistr, real_value, builderSingle.to_registers()))
                idRegistr += const.FLOAT_SIZE
            if slaveId==3:
                real_value = slaveId + random.randint(1, 100)
                builder.add_16bit_uint(real_value)
                builderSingle.add_16bit_uint(real_value)
                registers.append(RegistrInfo.RegistrInfo(idRegistr, real_value, builderSingle.to_registers()))
                idRegistr += const.INT_SIZE
            else:
                pass
            builderSingle.reset()
        try:
            report.createWS(name="Slave{0}".format(slaveId),index=slaveId)
            report.setValues(registers)
            report.save()
        except:
            log.error(report.errMessage)

        if slaveId==3:
            address=3
            block = ModbusSequentialDataBlock(address,builder.to_registers())
            di = ModbusSequentialDataBlock(address, [0] * const.MAX_DI)
            co = ModbusSequentialDataBlock(address, [0] * const.MAX_CO)
            hr = ModbusSequentialDataBlock(address, [0] * const.MAX_HR)
            store = ModbusSlaveContext(di=di, co=co, hr=hr, ir=block)
        if slaveId==2:
            address=2
            block = ModbusSequentialDataBlock(address,builder.to_registers())
            di = ModbusSequentialDataBlock(address, [0] * const.MAX_DI)
            co = ModbusSequentialDataBlock(address, [0] * const.MAX_CO)
            ir = ModbusSequentialDataBlock(address, [0] * const.MAX_IR)
            store = ModbusSlaveContext(di=di, co=co, hr=block, ir=ir)
        if slaveId == 1:
            address=1
            block = ModbusSequentialDataBlock(address,builder.to_registers())
            di = ModbusSequentialDataBlock(address, [0] * const.MAX_DI)
            co = ModbusSequentialDataBlock(address, [0] * const.MAX_CO)
            ir = ModbusSequentialDataBlock(address, [0] * const.MAX_IR)
            store = ModbusSlaveContext(di=di, co=co, hr=block, ir=ir)
        builder.reset()
        context.__setitem__(slaveId,store)
    # ----------------------------------------------------------------------- #
    # initialize the server information
    # ----------------------------------------------------------------------- #
    # If you don't set this or any fields, they are defaulted to empty strings.
    # ----------------------------------------------------------------------- #
    identity = ModbusDeviceIdentification()
    identity.VendorName = 'Pymodbus'
    identity.ProductCode = 'PM'
    identity.VendorUrl = 'http://github.com/riptideio/pymodbus/'
    identity.ProductName = 'Pymodbus Server'
    identity.ModelName = 'Pymodbus Server'
    identity.MajorMinorRevision = '2.3.0'
    # ----------------------------------------------------------------------- #
    # run the server you want
    # ----------------------------------------------------------------------- #
    # address=1
    # time = 1  # 5 seconds delay
    # loop = LoopingCall(f=updateReads, a=(context,))
    # loop.start(time, now=False)  # initially delay by time
    # log.debug("slave_id;addr;value_64float\n")
    StartTcpServer(context, identity=identity, address=("localhost", 5020))


if __name__ == "__main__":
    run_server()
