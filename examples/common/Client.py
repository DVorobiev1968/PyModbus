#!/usr/bin/env python
# --------------------------------------------------------------------------- #
# import the various server implementations
# --------------------------------------------------------------------------- #
import random
from threading import Thread

from openpyxl.utils.exceptions import InvalidFileException

from ExcelLib.RegistrInfo import RegistrInfo
from ExcelLib.ReportFromExcel import ReportFromExcel
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from ExcelLib.Constans import Constans
# from pymodbus.client.sync import ModbusUdpClient as ModbusClient
# from pymodbus.client.sync import ModbusSerialClient as ModbusClient
import time
# --------------------------------------------------------------------------- #
# configure the client logging
# --------------------------------------------------------------------------- #
import logging

FORMAT = ('%(asctime)-15s %(threadName)-15s '
          '%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
# log.setLevel(logging.DEBUG)
log.setLevel(logging.INFO)
# log.setLevel(logging.WARNING)

UNIT = 0x0


def readInputRegisters16(address, unit=1):
    client = ModbusClient('localhost', port=5020)
    client.connect()
    errno = 0
    try:
        response = client.read_input_registers(address, 1, unit=unit)
        if not response.isError():
            decoder = BinaryPayloadDecoder.fromRegisters(response.registers,
                                                         byteorder=Endian.Little,
                                                         wordorder=Endian.Little)
            value16Bit = decoder.decode_16bit_int()
            log.info("Read 16 bit value: {0:d} in register: {1:d}".format(value16Bit, address))
            time.sleep(1)
            decoder.reset()
            return value16Bit
    except Exception:
        log.error("{0}".format(errno))
    client.close()


def readCycleInputRegisters16(address, unit=1):
    client = ModbusClient('localhost', port=5020)
    client.connect()
    errno = 0
    while True:
        try:
            response = client.read_input_registers(address, 1, unit=unit)
            if not response.isError():
                decoder = BinaryPayloadDecoder.fromRegisters(response.registers,
                                                             byteorder=Endian.Little,
                                                             wordorder=Endian.Little)
                value16Bit = decoder.decode_16bit_int()
                log.info("Read 16 bit value: {0:d} in register: {1:d}".format(value16Bit, address))
                time.sleep(1)
                decoder.reset()
        except Exception:
            log.error("{0}".format(errno))
            break
    client.close()


def readHoldingRegisters16(address,unit):
    client = ModbusClient('localhost', port=5020)
    client.connect()
    errno = 0
    try:
        response = client.read_holding_registers(address, 1, unit=unit)
        if not response.isError():
            decoder = BinaryPayloadDecoder.fromRegisters(response.registers,
                                                         byteorder=Endian.Little,
                                                         wordorder=Endian.Little)
            value16Bit = decoder.decode_16bit_int()
            log.info("Read 16 bit value: {0:d} in register: {1:d}".format(value16Bit, address))
            time.sleep(1)
            decoder.reset()
            return value16Bit
    except Exception:
        log.error("{0}".format(errno))
    client.close()


def readSingleHoldingRegisters32(address, unit=UNIT):
    client = ModbusClient('localhost', port=5020)
    client.connect()
    errno = 0
    try:
        response = client.read_holding_registers(address, 2, unit=unit)
        if not response.isError():
            decoder = BinaryPayloadDecoder.fromRegisters(response.registers,
                                                         byteorder=Endian.Little,
                                                         wordorder=Endian.Little)
            value32Float = decoder.decode_32bit_float()
            log.info("Read 32 bit value: {0:4.20f} in register: {1:d}".format(value32Float, address))
            time.sleep(1)
            decoder.reset()
    except Exception:
        log.error("{0}".format(errno))
    client.close()


def readMultiHoldingRegisters32(startAddr, endAddr, unit=UNIT):
    const = Constans()
    client = ModbusClient('localhost', port=5020)
    client.connect()
    address = startAddr
    report = ReportFromExcel()
    registers = []
    try:
        while address < endAddr:
            response = client.read_holding_registers(address=address, count=const.FLOAT_SIZE, unit=unit)
            failResponse = response.isError()
            if not failResponse:
                decoder = BinaryPayloadDecoder.fromRegisters(response.registers,
                                                             byteorder=Endian.Little,
                                                             wordorder=Endian.Little)
                value32Float = decoder.decode_32bit_float()
                log.debug("Read 32 bit value: {0:4.10f} in register: {1:d}".format(value32Float, address))
                registers.append(RegistrInfo(address, value32Float, response.registers))
                # time.sleep(0.02)
                decoder.reset()
                address += const.FLOAT_SIZE
            else:
                break
        if registers.__len__() > 0:
            nameWS = "Slave{0}".format(unit)
            report.openWB(nameFile='Report.xlsx', sheet=nameWS)
            report.addValues(listObj=registers, startColumn=4)
            report.save()
    except (KeyError, TypeError, InvalidFileException) as err:
        log.error(err.__str__())
    client.close()


def readHoldingRegisters32(address):
    client = ModbusClient('localhost', port=5020)
    client.connect()
    errno = 0
    while True:
        try:
            response = client.read_holding_registers(address, 2)
            if not response.isError():
                decoder = BinaryPayloadDecoder.fromRegisters(response.registers,
                                                             byteorder=Endian.Little,
                                                             wordorder=Endian.Little)
                value32Float = decoder.decode_32bit_float()
                log.info("Read 32 bit value: {0:4.10f} in register: {1:d}".format(value32Float, address))
                time.sleep(1)
                decoder.reset()
        except Exception:
            log.error("{0}".format(errno))
            break
    client.close()


def readSingleHoldingRegister64(address, unit=UNIT):
    client = ModbusClient('localhost', port=5020)
    client.connect()
    errno = 0
    try:
        response = client.read_holding_registers(address, 4, unit=unit)
        if not response.isError():
            decoder = BinaryPayloadDecoder.fromRegisters(response.registers,
                                                         byteorder=Endian.Little,
                                                         wordorder=Endian.Little)
            value64Float = decoder.decode_64bit_float()
            log.info("Read 64 bit value: {0} in register: {1:d}".format(value64Float, address))
            time.sleep(1)
            decoder.reset()
    except Exception:
        log.error("{0}".format(errno))
    client.close()


def readMultiHoldingRegisters64(startAddr, endAddr, unit=UNIT):
    const = Constans()
    client = ModbusClient('localhost', port=5020)
    client.connect()
    address = startAddr
    report = ReportFromExcel()
    registers = []
    try:
        while address < endAddr:
            response = client.read_holding_registers(address=address, count=const.DOUBLE_SIZE, unit=unit)
            failResponse = response.isError()
            if not failResponse:
                decoder = BinaryPayloadDecoder.fromRegisters(response.registers,
                                                             byteorder=Endian.Little,
                                                             wordorder=Endian.Little)
                value64Float = decoder.decode_64bit_float()
                log.debug("Read 64 bit value: {0:4.15f} in register: {1:d}".format(value64Float, address))
                registers.append(RegistrInfo(address, value64Float, response.registers))
                # time.sleep(0.02)
                decoder.reset()
                address += const.DOUBLE_SIZE
            else:
                break
        if registers.__len__() > 0:
            nameWS = "Slave{0}".format(unit)
            report.openWB(nameFile='Report.xlsx', sheet=nameWS)
            report.addValues(listObj=registers, startColumn=4)
            report.save()
    except (KeyError, TypeError, InvalidFileException) as err:
        log.error(err.__str__())
    client.close()


def readHoldingRegisters64(address):
    client = ModbusClient('localhost', port=5020)
    client.connect()
    errno = 0
    while True:
        try:
            response = client.read_holding_registers(address, 4)
            if not response.isError():
                decoder = BinaryPayloadDecoder.fromRegisters(response.registers,
                                                             byteorder=Endian.Little,
                                                             wordorder=Endian.Little)
                value64Float = decoder.decode_64bit_float()
                log.info("Read 64 bit value: {0:4.10f} in register: {1:d}".format(value64Float, address))
                time.sleep(1)
                decoder.reset()
        except Exception:
            log.error("{0}".format(errno))
            break
    client.close()


def writeRegister16(address, unit=3):
    client = ModbusClient('localhost', port=5020)
    client.connect()
    iValue = random.randint(1, 65535)
    log.info("Write 16 bit value: {0:d} in register: {1:d}".format(iValue, address))
    request = client.write_register(address, iValue, unit=unit)
    if not request.isError():
        time.sleep(1)
    else:
        log.error("Error write_registers:")
    client.close()


def writeHR16(address, iValue, unit):
    client = ModbusClient('localhost', port=5020)
    client.connect()
    log.info("Write 16 bit value: {0:d} in register: {1:d}".format(iValue, address))
    request = client.write_register(address, iValue, unit=unit)
    if not request.isError():
        time.sleep(1)
    else:
        log.error("Error write_registers:")
    client.close()


def writeRegister32(address):
    client = ModbusClient('localhost', port=5020)
    client.connect()
    while True:
        builder = BinaryPayloadBuilder(byteorder=Endian.Little,
                                       wordorder=Endian.Little)
        real_value = random.randint(1, 100) * 1000 / 3
        log.info("Write 32 bit value: {0:4.10f} in register: {1:d}".format(real_value, address))
        builder.add_32bit_float(real_value)
        payload = builder.to_registers()
        payload = builder.build()
        request = client.write_registers(address, payload, skip_encode=True, unit=UNIT)
        if not request.isError():
            time.sleep(1)
            builder.reset()
        else:
            log.error("Error write_registers:")
            break
    client.close()


def writeSingleRegister32(address):
    client = ModbusClient('localhost', port=5020)
    client.connect()
    builder = BinaryPayloadBuilder(byteorder=Endian.Little,
                                   wordorder=Endian.Little)
    real_value = random.randint(1, 100) * 1000 / 3
    log.info("Write 32 bit value: {0:4.10f} in register: {1:d}".format(real_value, address))
    builder.add_32bit_float(real_value)
    payload = builder.to_registers()
    payload = builder.build()
    request = client.write_registers(address, payload, skip_encode=True, unit=UNIT)
    if not request.isError():
        time.sleep(1)
        builder.reset()
    else:
        log.error("Error write_registers:")
    client.close()


def writeRegister64(address):
    client = ModbusClient('localhost', port=5020)
    client.connect()
    while True:
        builder = BinaryPayloadBuilder(byteorder=Endian.Little,
                                       wordorder=Endian.Little)
        real_value = random.randint(1, 100) * 1000 / 3
        log.info("Write 64 bit value: {0:4.10f} in register: {1:d}".format(real_value, address))
        builder.add_64bit_float(real_value)
        payload = builder.to_registers()
        payload = builder.build()
        request = client.write_registers(address, payload, skip_encode=True, unit=UNIT)
        if not request.isError():
            time.sleep(1)
            builder.reset()
        else:
            log.error("Error write_registers:")
            break
    client.close()


def run_client_2():
    client = ModbusClient('localhost', port=5020)
    client.connect()
    log.debug("Read Write 32 and 64 bit value to holding registers")
    builder = BinaryPayloadBuilder(byteorder=Endian.Little,
                                   wordorder=Endian.Little)
    for i in range(0, 9):
        real_value = random.randint(1, 100) / 3
        builder.add_64bit_float(real_value)
        payload = builder.to_registers()
        payload = builder.build()
        rq = client.write_registers(i, payload, skip_encode=True, unit=UNIT)
        if not rq.isError():
            time.sleep(1)
            builder.reset()
        else:
            log.error("Error write_registers:")
            break
    client.close()


def run_client_1():
    client = ModbusClient('localhost', port=5020)
    client.connect()
    log.debug("Read Write to multiple holding registers")
    for i in range(1, 20):
        log.debug("Write to multiple holding registers and read back")
        real_value = i
        rq = client.write_register(i, real_value, unit=UNIT)
        assert (not rq.isError())  # test that we are not an error
        time.sleep(1)
    client.close()


def run_client():
    client = ModbusClient('localhost', port=5020)
    client.connect()
    log.debug("Read to multiple holding registers")
    for i in range(1, 20):
        rr = client.read_holding_registers(1, 10, unit=UNIT)
        for j in range(1, 10): print("{0:f};".format(rr.registers[j]))
        print("\n")
        time.sleep(1)
    client.close()


def runReadCoil(address):
    global client
    log.debug("Reading Coils")
    rr = client.read_coils(address, 1, unit=UNIT)
    log.debug(rr)


def runWriteCoil(address, value=True):
    global client
    log.debug("Write to a Coil and read back")
    rq = client.write_coil(address, value, unit=UNIT)
    rr = client.read_coils(address, 1, unit=UNIT)
    assert (not rq.isError())  # test that we are not an error
    assert (rr.bits[0] == value)  # test the expected value
    log.debug(rr)


def runCycleWriteCoil(address, count=1):
    client = ModbusClient('localhost', port=5020)
    client.connect()
    while True:
        log.debug("Write to a Coil address:{0:d} and read back".format(address))
        rr = client.read_coils(address, count, unit=UNIT)
        if not rr.isError():
            time.sleep(0.2)
            value = not rr.bits[0]
            for i in range(0, count):
                rq = client.write_coil(address + i, value, unit=UNIT)
                value = not value
                assert (not rq.isError())  # test that we are not an error
                log.debug(rq)
                if rq.isError():
                    log.error("Error write_registers:")
                    break
        else:
            log.error("Error write_registers:")
            break
    client.close()


def runWriteRegister(address, value=0):
    global client
    log.debug("Write to a input register and read back")

    def writeRegister(address, value):
        log.debug("Write 16 bit value: {0:d} in register: {1:d}".format(value, address))
        request = client.write_register(address, value, unit=UNIT)
        if not request.isError():
            time.sleep(1)
        else:
            log.error("Error write_registers:")

    writeRegister(address, value)
    readInputRegister(address)


def runWriteHoldingRegister(address, value=0):
    global client
    writeHoldRegister(address)
    readHoldingRegister(address)


def readInputRegister(address):
    global client
    errno = 0
    try:
        response = client.read_input_registers(address)
        if not response.isError():
            decoder = BinaryPayloadDecoder.fromRegisters(response.registers,
                                                         byteorder=Endian.Little,
                                                         wordorder=Endian.Little)
            value16Bit = decoder.decode_16bit_int()
            log.debug("Read 16 bit value: {0:d} in register: {1:d}".format(value16Bit, address))
            decoder.reset()
            return value16Bit
    except Exception:
        log.error("{0}".format(errno))


def readHoldingRegister(address):
    global client
    errno = 0
    try:
        response = client.read_holding_registers(address, 4)
        if not response.isError():
            decoder = BinaryPayloadDecoder.fromRegisters(response.registers,
                                                         byteorder=Endian.Little,
                                                         wordorder=Endian.Little)
            value64Float = decoder.decode_64bit_float()
            log.info("Read 64 bit value: {0:4.10f} in register: {1:d}".format(value64Float, address))
            decoder.reset()
    except Exception:
        log.error("{0}".format(errno))


def writeHoldRegister(address):
    global client
    builder = BinaryPayloadBuilder(byteorder=Endian.Little,
                                   wordorder=Endian.Little)
    real_value = random.randint(1, 100) * 1000 / 3
    log.info("Write 64 bit value: {0:4.10f} in register: {1:d}".format(real_value, address))
    builder.add_64bit_float(real_value)
    payload = builder.to_registers()
    payload = builder.build()
    request = client.write_registers(address, payload, skip_encode=True, unit=UNIT)
    if not request.isError():
        time.sleep(1)
        builder.reset()
    else:
        log.error("Error write_registers:")


def readWriteMulti():
    client = ModbusClient('localhost', port=5020)
    client.connect()
    startAddress = 10
    count = 1
    value = 20
    arguments = {
        'read_address': startAddress,
        'read_count': count,
        'write_address': startAddress + count * 2,
        'write_registers': value,
    }
    log.debug("Read write registeres simulataneously")
    rq = client.readwrite_registers(unit=UNIT, **arguments)
    assert (not rq.isError())  # test that we are not an error
    rr = client.read_holding_registers(startAddress, (count + 1) * 2, unit=UNIT)
    if not rr.isError():
        for register in rr.registers:
            print("{0:f};".format(register))
        print("\n")

    client.close()


def run_sync_client():
    client = ModbusClient('localhost', port=5020)
    client.connect()
    log.debug("Reading Coils")
    rr = client.read_coils(1, 1, unit=UNIT)
    log.debug(rr)

    log.debug("Write to a Coil and read back")
    rq = client.write_coil(0, True, unit=UNIT)
    rr = client.read_coils(0, 1, unit=UNIT)
    assert (not rq.isError())  # test that we are not an error
    assert (rr.bits[0] == True)  # test the expected value

    log.debug("Write to multiple coils and read back- test 1")
    rq = client.write_coils(1, [True] * 8, unit=UNIT)
    assert (not rq.isError())  # test that we are not an error
    rr = client.read_coils(1, 21, unit=UNIT)
    assert (not rr.isError())  # test that we are not an error
    resp = [True] * 21

    # If the returned output quantity is not a multiple of eight,
    # the remaining bits in the final data byte will be padded with zeros
    # (toward the high order end of the byte).

    # resp.extend([False]*3)
    # assert(rr.bits == resp)         # test the expected value

    log.debug("Write to multiple coils and read back - test 2")
    rq = client.write_coils(1, [False] * 8, unit=UNIT)
    rr = client.read_coils(1, 8, unit=UNIT)
    assert (not rq.isError())  # test that we are not an error
    assert (rr.bits == [False] * 8)  # test the expected value

    log.debug("Read discrete inputs")
    rr = client.read_discrete_inputs(0, 8, unit=UNIT)
    assert (not rq.isError())  # test that we are not an error

    log.debug("Write to a holding register and read back")
    rq = client.write_register(10, 10, unit=UNIT)
    rr = client.read_holding_registers(10, 1, unit=UNIT)
    assert (not rq.isError())  # test that we are not an error
    assert (rr.registers[0] == 10)  # test the expected value
    print("rr:{0:f}\n".format(rr.registers[0]))

    log.debug("Write to multiple holding registers and read back")
    rq = client.write_registers(1, [10] * 8, unit=UNIT)
    rr = client.read_holding_registers(1, 8, unit=UNIT)
    assert (not rq.isError())  # test that we are not an error
    assert (rr.registers == [10] * 8)  # test the expected value
    for i in range(1, 8): print("{0:f};".format(rr.registers[i]))
    print("\n")

    log.debug("Read input registers")
    rr = client.read_input_registers(1, 8, unit=UNIT)
    assert (not rq.isError())  # test that we are not an error

    arguments = {
        'read_address': 10,
        'read_count': 1,
        'write_address': 12,
        'write_registers': 20,
    }
    log.debug("Read write registeres simulataneously")
    rq = client.readwrite_registers(unit=UNIT, **arguments)
    rr = client.read_holding_registers(1, 3, unit=UNIT)

    # ----------------------------------------------------------------------- #
    # close the client
    # ----------------------------------------------------------------------- #
    client.close()


def cycleReadWriteIR(address=100,itteration=0):
    if itteration==0:
        while True:
            iValue=readHoldingRegisters16(100,3)
            iValue+=1
            writeHR16(100,iValue,3)
    else:
        while itteration>0  :
            iValue=readHoldingRegisters16(100,3)
            writeHR16(100,iValue,3)
            itteration-=1

def cycleReadWrite16(address):
    threadWrite = Thread(target=writeRegister16, args=(address))
    threadRead = Thread(target=readHoldingRegisters16, args=(address))
    threadWrite.start()
    threadRead.start()
    threadWrite.join(10)
    threadRead.join(10)


def cycleReadWrite32(addrSrc, addrTarget):
    threadWrite = Thread(target=writeRegister32, args=(addrSrc))
    threadRead = Thread(target=readHoldingRegisters32, args=(addrTarget))
    threadWrite.start()
    threadRead.start()
    threadWrite.join(10)
    threadRead.join(10)


def cycleReadWrite64(addrSrc, addrTarget):
    threadWrite = Thread(target=writeRegister64, args=(addrSrc))
    threadRead = Thread(target=readHoldingRegisters64, args=(addrTarget))
    threadWrite.start()
    threadRead.start()
    threadWrite.join(10)
    threadRead.join(10)


def singleReadWrite():
    global client
    client = ModbusClient('localhost', port=5020)
    client.connect()
    addressCoil = 1
    addressRegister = 10
    addressHoldingRegister = 20
    runWriteCoil(addressCoil + 1, True)
    time.sleep(1)
    runWriteCoil(addressCoil, True)
    time.sleep(1)
    runWriteRegister(addressRegister, 999)
    time.sleep(1)
    runWriteHoldingRegister(addressHoldingRegister, 70000)
    client.close()


if __name__ == "__main__":
    # readSingleHoldingRegister64(1)
    # readSingleHoldingRegister64(1,2)
    # singleReadWrite()
    # writeRegisters(20)
    # writeRegister16(10)
    # writeSingleRegister32(12)
    # readMultiHoldingRegisters32(1,500,1)
    # readMultiHoldingRegisters64(1,1000,2)
    # readSingleHoldingRegisters32(1,1)
    # readSingleHoldingRegisters32(1,1)
    # readSingleHoldingRegister64(1,2)
    # writeRegister32(12)
    # run_sync_client()
    # readWriteMulti()
    # runCycleWriteCoil(1,2)
    # writeHoldRegister(20)
    # cycleReadWrite16({10})
    # cycleReadWrite32({12},{10})
    # cycleReadWrite64({20})
    # readHoldingRegisters16(1)
    # writeRegister16(2, iValue=0)
    # readInputRegisters16(2, 3)
    # readInputRegisters16(3,3)
    cycleReadWriteIR()
