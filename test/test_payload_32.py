#!/usr/bin/env python
"""
Payload Utilities Test Fixture
--------------------------------
This fixture tests the functionality of the payload
utilities.

* PayloadBuilder
* PayloadDecoder
"""
import unittest
from pymodbus.exceptions import ParameterException
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadBuilder, BinaryPayloadDecoder
import logging

FORMAT = ('%(asctime)-15s %(threadName)-15s'
          ' %(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.DEBUG)

#---------------------------------------------------------------------------#
# Fixture
#---------------------------------------------------------------------------#
class ModbusPayloadUtilityTests(unittest.TestCase):

    # ----------------------------------------------------------------------- #
    # Setup/TearDown
    # ----------------------------------------------------------------------- #

    def setUp(self):
        """
        Initializes the test environment and builds request/result
        encoding pairs
        """
        pass
    def tearDown(self):
        """ Cleans up the test environment """
        pass

    # ----------------------------------------------------------------------- #
    # Payload Builder Tests
    # ----------------------------------------------------------------------- #

    def testAdd32BitFloat(self):
        """ Test basic bit message encoding/decoding """
        builder = BinaryPayloadBuilder(byteorder=Endian.Little,
                                       wordorder=Endian.Little)
        builder.add_32bit_float(1.25)
        decoder = BinaryPayloadDecoder.fromRegisters(builder.to_registers(),
                                       byteorder=Endian.Little,
                                       wordorder=Endian.Little)
        self.assertEqual(1.25,   decoder.decode_32bit_float())

#---------------------------------------------------------------------------#
# Main
#---------------------------------------------------------------------------#
if __name__ == "__main__":
    unittest.main()
