from ExcelLib.Constans import Constans
class RegistrInfo():
    def __init__(self,addr,val,registers=[]):
        const=Constans()
        self.size32=const.FLOAT_SIZE
        self.size64=const.DOUBLE_SIZE
        self.addr=addr
        self.dValue=val
        self.registers=registers

    def __len__(self):
        return 3