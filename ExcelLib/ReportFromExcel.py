# -*- coding: utf-8 -*-
import os
from openpyxl import load_workbook
from openpyxl.utils import get_column_letter


from definitions import ROOT_DIR,REPORT_DIR
from ExcelLib.RegistrInfo import RegistrInfo

from openpyxl import Workbook

class ReportFromExcel():
    """
    Класс создает объект рабочая книга
    """
    def __init__(self, nameFile='Report.xlsx'):
        """
        Активный лист переименовывываем в Data
        задаем атрибут для отрисовки неактивного листа

        :param: * namefile: имя файла в который будут выгружены данные
        """
        self.cwd=os.getcwd()
        self.nameFile = nameFile
        self.pathReportFile=os.path.join(REPORT_DIR,nameFile)
        self.wb=Workbook()
        self.errMessage=""

    def __repr__(self):
        return "cwd:{0};nameFile:{1};pathReportFile:{2}".format(self.cwd,self.nameFile,self.pathReportFile)
    def __str__(self):
        return "cwd:{0};nameFile:{1};pathReportFile:{2}".format(self.cwd,self.nameFile,self.pathReportFile)

    def settings(self):
        pass

    def save(self):
        try:
            if not os.path.exists(REPORT_DIR):
                os.mkdir(REPORT_DIR)
            self.wb.save(self.pathReportFile)
        except (PermissionError) as err:
            self.errMessage="{0}:{1}".format(err.__class__.__name__,err)


    def setDemoValues(self):
        for row in range(1,10):
            for col in range(1,10):
                _=self.ws.cell(column=col, row=row,value="{0}".format(get_column_letter(col)))

    def setValues(self,listObj):
        i=0
        row=1
        while i < listObj.__len__():
            _=self.ws.cell(column=1,row=row,value=listObj.__getitem__(i).addr)
            _=self.ws.cell(column=2,row=row,value=listObj.__getitem__(i).dValue)
            _=self.ws.cell(column=3,row=row,value="{0}".format(listObj.__getitem__(i).registers))
            i+=1
            row+=1

    def addValues(self,listObj,startColumn):
        i=0
        row=1
        while i < listObj.__len__():
            _=self.ws.cell(column=startColumn,row=row,value=listObj.__getitem__(i).dValue)
            _=self.ws.cell(column=startColumn+1,row=row,value="{0}".format(listObj.__getitem__(i).registers))
            i+=1
            row+=1

    def  createWS(self,name='Slave1',index=1):
        try:
            self.ws=self.wb.get_sheet_by_name(name)
        except KeyError as err:
            self.ws=self.wb.create_sheet(name,index)
            self.ws.sheet_properties.tabColor="1072BA"
            self.ws.title=name
            self.errMessage=err.__str__()


    def openWB(self,nameFile,sheet):
        nameFile=os.path.join(REPORT_DIR,nameFile)
        self.wb=load_workbook(nameFile)
        self.createWS(sheet)
